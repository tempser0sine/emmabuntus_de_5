# Outil à installer :
# sudo apt-get install debhelper cdbs lintian build-essential fakeroot devscripts pbuilder dh-make debootstrap
#
# https://www.debian-fr.org/t/extraire-paquet-deb/31870
#
# Pour extraire le paquet :
#
# dpkg-deb -x paquet.deb repertoire -> extrait l'arborescence
# dpkg-deb -e paquet.deb -> extrait le répertoire DEBIAN contenant les différents fichiers postinst, control, etc
#
# Pour assembler le paquet :
#
# dpkg-deb -b repertoire paquet.deb
#

# Méthode paquet origine

version=160_amd64
nom_paquet=partman-auto
ext_paquet=udeb

mkdir ${nom_paquet}
dpkg-deb -x ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}
dpkg-deb -e ${nom_paquet}_${version}.${ext_paquet} ${nom_paquet}/DEBIAN

# A faire dans les fichiers :
modifier fichier lib/partman/recipes/50home et lib/partman/recipes-amd64-efi/50home :
remplacer cette ligne ... par ...
pour / 1500 6000 30000 $default_filesystem 15000 20000 30000 $default_filesystem
pour /home 1000 10000 -1 $default_filesystem 3000 10000 -1 $default_filesystem

modifier fichier lib/partman/recipes/80multi et lib/partman/recipes-amd64-efi/80multi :
remplacer cette ligne
pour /  1000 1500 10000 $default_filesystem  15000 20000 30000 $default_filesystem
pour /var 1000 1500 10000 $default_filesystem 3000 5000 10000 $default_filesystem

modifier fichier lib/partman/recipes/30atomic et lib/partman/recipes-amd64-efi/30atomic :
pour /  900 10000 -1 $default_filesystem 15000 20000 -1 $default_filesystem

# Voir doc : https://wikitech.wikimedia.org/wiki/PartMan/Auto

# Méthode assemblage

dpkg-deb -b ${nom_paquet} ${nom_paquet}_${version}_mod_emma.${ext_paquet}
cp ${nom_paquet}_${version}_mod_emma.${ext_paquet} ../../../arch/64/packages.installer_debian-installer.udeb/${nom_paquet}_${version}.${ext_paquet}
