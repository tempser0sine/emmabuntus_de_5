#! /bin/bash

# steam_wrapper.sh --
#
#   This file permits to install nofree softwares for the Emmabuntüs Equitable Distribs.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="Steam"
nom_paquet=steam
nom_logiciel_exe=steam
logiciel=""

nom_script=install_steam.sh

dir_install_non_free_softwares=/opt/Install_non_free_softwares
delai_fenetre=20

logiciel=$1

#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi


if [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then

    export WINDOW_DIALOG_INSTALL_NO_INSTALL_LIVE='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_no_install_live'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_DIALOG_INSTALL_NO_INSTALL_LIVE="$(gtkdialog --center --program=WINDOW_DIALOG_INSTALL_NO_INSTALL_LIVE)"

    exit 1

fi


if [[ $(uname -m | grep -e "x86_64") ]] ; then

    if [[ $(which $nom_logiciel_exe) ]]
    then

        echo "# $nom_logiciel_affichage is already installed"

        $nom_logiciel_exe ${logiciel}

        exit 0

    fi

    export WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-question" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_demarrage_soft_non_libre_steam'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">
    <button cancel></button>
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE="$(gtkdialog --center --program=WINDOW_DIALOG_INSTALL_NO_FREE_SOFTWARE)"

    eval ${MENU_DIALOG_DIALOG_INSTALL_NO_FREE_SOFTWARE}

    if [[ ${EXIT} != "OK" ]]
    then

        echo "# $nom_logiciel_affichage software installation canceled"

        exit 1

    else


        if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

            echo "Internet is a live"

        else

            echo "Internet wasn't a live !!"

            export WINDOW_DIALOG_NO_INTERNET_INSTALL_KO='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-dialog-error" resizable="false">
            <vbox spacing="0">

            <text use-markup="true" wrap="false" xalign="0" justify="3">
            <input>echo "'$message_non_internet_install_ko'" | sed "s%\\\%%g"</input>
            </text>

            <hbox spacing="10" space-expand="false" space-fill="false">
            <button can-default="true" has-default="true" use-stock="true" is-focus="true">
            <label>gtk-ok</label>
            <action>exit:OK</action>
            </button>
            </hbox>

            </vbox>
            </window>'

            MENU_DIALOG_DIALOG_NO_INTERNET_INSTALL_KO="$(gtkdialog --center --program=WINDOW_DIALOG_NO_INTERNET_INSTALL_KO)"

            exit 1

        fi

        pkexec $dir_install_non_free_softwares/$nom_script

        if [[ $(which $nom_logiciel_exe) ]]
        then
            $nom_logiciel_exe ${logiciel}
        else
            zenity --error --timeout=$delai_fenetre \
                --text="<span color=\"red\">$install_ko</span>"
        fi

    fi

else

    echo "# $nom_logiciel_affichage is not instable version 32 bits not available"

    exit 2

fi

