# calibre wide preferences

### Begin group: DEFAULT
 
# database path
# Chemin vers la base de données dans laquelle les livres sont stockés
database_path = ''
 
# filename pattern
# Modèle de détection de métadonnées à partir de noms de fichiers.
filename_pattern = u'(?P<title>.+) - (?P<author>[^_]+)'
 
# isbndb com key
# Clef d’accès pour isbndb.com
isbndb_com_key = ''
 
# network timeout
# Délai d’attente par défaut pour les opérations réseaux (en secondes)
network_timeout = 5
 
# library path
# Chemin vers le dossier où est enregistrée votre bibliothèque de livres
library_path = u'/usr/share/Documentation_emmabuntus/Documents/eBooks'
 
# language
# La langue à utiliser pour l’affichage de l’interface utilisateur
language = ''
 
# output format
# Le format de sortie par défaut pour les conversions de livre numérique.
output_format = 'epub'
 
# input format order
# Liste triée de formats à privilégier pour l’entrée.
input_format_order = cPickle.loads('\x80\x02]q\x01(U\x04EPUBq\x02U\x04AZW3q\x03U\x04MOBIq\x04U\x03LITq\x05U\x03PRCq\x06U\x03FB2q\x07U\x04HTMLq\x08U\x03HTMq\tU\x04XHTMq\nU\x05SHTMLq\x0bU\x05XHTMLq\x0cU\x03ZIPq\rU\x03ODTq\x0eU\x03RTFq\x0fU\x03PDFq\x10U\x03TXTq\x11e.')
 
# read file metadata
# Lire les métadonnées à partir des fichiers
read_file_metadata = True
 
# worker process priority
# La priorité des processus de travail autonomes. Une priorité supérieure signifie qu’ils fonctionnent plus vite et consomment plus de ressources. La plupart des taches comme la conversion/le téléchargement des actualités/l’ajout des livres/etc sont affectées par ce réglage.
worker_process_priority = 'normal'
 
# swap author names
# Intervertir les prénoms et noms de l’auteur lors de la lecture des métadonnées
swap_author_names = False
 
# add formats to existing
# Ajouter des nouveaux formats aux enregistrements existants de livres
add_formats_to_existing = False
 
# check for dupes on ctl
# Vérifier les doublons lors de la copie vers une autre librairie
check_for_dupes_on_ctl = False
 
# installation uuid
# Installation UUID
installation_uuid = '342299e5-de35-4d8b-9f0a-c1a1baf3a095'
 
# new book tags
# Étiquettes à attacher aux livres ajoutés dans la bibliothèque
new_book_tags = cPickle.loads('\x80\x02]q\x01.')
 
# mark new books
# Marquer les livres nouvellement ajoutés. La marque est temporaire et sera automatiquement supprimée quand calibre sera redémarré.
mark_new_books = False
 
# saved searches
# Liste de noms de recherches sauvegardées
saved_searches = cPickle.loads('\x80\x02}q\x01.')
 
# user categories
# Catégories du Navigateur d’étiquettes créées par l’utilisateur
user_categories = cPickle.loads('\x80\x02}q\x01.')
 
# manage device metadata
# Comment et quand calibre met à jour les métadonnées dans le périphérique.
manage_device_metadata = 'manual'
 
# limit search columns
# Quand vous recherchez du texte sans utiliser de préfixe de recherche, comme par exemple, Rouge au lieu de titre:Rouge, les colonnes recherchées sont limitées à celles nommées ci-dessous.
limit_search_columns = False
 
# limit search columns to
# Indiquez les colonnes dans lesquelles s'effectuera la recherche quand aucun préfixe ne sera spécifié (par exemple, lorsque la  recherche est "Rouge" et non "title:Rouge". Entrez une liste de noms de colonne, séparés par des virgules. N'aura d'effet que si vous activez l'option ci-dessus pour limiter les colonnes à chercher.
limit_search_columns_to = cPickle.loads('\x80\x02]q\x01(U\x05titleq\x02U\x07authorsq\x03U\x04tagsq\x04U\x06seriesq\x05U\tpublisherq\x06e.')
 
# use primary find in search
# Les caractères saisis dans le champ de recherche correspondront à leurs versions accentuées, en fonction de la langue que vous avez choisie pour l'interface de calibre. Par exemple, en anglais, rechercher n correspondra à ñ et n, mais si votre langue est l'espagnol il correspondra seulement à n. Notez que ceci est beaucoup plus lent qu'une simple recherche sur des bibliothèques très volumineuse. Aussi, cette option n'aura pas d'effet si vous activez la recherche sensible à la casse
use_primary_find_in_search = True
 
# case sensitive
# Rendre les recherches sensibles à la casse
case_sensitive = False
 
# migrated
# For Internal use. Don't modify.
migrated = False
 


